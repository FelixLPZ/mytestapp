import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchApiServiceService {

  constructor( private http: HttpClient) { }

  url = 'https://api.github.com/search/repositories?q=framework+language:javascript&page=1&per_page=100&sort=stars&order=desc';

  getFrameworks() {
    return this.http.get(this.url).pipe(
      map(response => {
          return response['items'];
      })
    );
  }



  getName(name1) {
    /* return this.http.get(this.url, {headers: {name: name1}}).pipe(
        map(response => {
            return response['items'];
        })
    ); */
    return this.http.get(this.url).pipe(
      map(response => {
          return response['items'];
      })
    );
  }
}

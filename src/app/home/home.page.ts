import { Component, OnInit } from '@angular/core';
import { Mock } from 'protractor/built/driverProviders';
import { SearchApiServiceService } from '../services/search-api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  mockSearchTermName = "framework1";
  mockFrameworks = [
    {
      name: 'framework1',
      url: 'https://www.framework1.com'
    },
    {
      name: 'framework2',
      url: 'https://www.framework2.com'
    }
  ];

  searchTerm: string;
  frameworks = [];
  // frameworks = this.mockFrameworks;

  constructor( private searchApiService: SearchApiServiceService) {}

  ngOnInit() {
    this.getFrameworks();
  }

  // call to the API to get all the frameworks
  getFrameworks() {
    this.searchApiService.getFrameworks().subscribe((response) => {
      this.frameworks = response;
      console.log(response);
    })
  }

  searchName() {
    this.searchApiService.getName(this.mockSearchTermName).subscribe((response) => {
     console.log(response);
    })
  }

  

}
